run("8-bit");
run("Grays");
run("Invert LUT");
setAutoThreshold("Otsu");
//run("Threshold...");
setAutoThreshold("Otsu");
call("ij.plugin.frame.ThresholdAdjuster.setMode", "B&W");
setAutoThreshold("Otsu");
//setThreshold(242, 255);
setOption("BlackBackground", false);
run("Convert to Mask");
run("Set Measurements...", "mean min");
roiManager("Show All");
run("Convert to Mask");
roiManager("Measure");

